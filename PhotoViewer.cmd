pushd "%~dp0"
reg.exe load "HKLM\WIM_Software" "wimmount\Windows\System32\config\software"

Reg add "HKLM\WIM_Software\Classes\Applications\photoviewer.dll\shell\open" /v "MuiVerb" /t REG_SZ /d "@photoviewer.dll,-3043" /f >nul
Reg add "HKLM\WIM_Software\Classes\Applications\photoviewer.dll\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\WIM_Software\Classes\Applications\photoviewer.dll\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\WIM_Software\Classes\Applications\photoviewer.dll\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-72" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Bmp" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3056" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Bmp" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Bmp\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-70" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Bmp\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Bmp\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Gif" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3057" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Gif" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Gif\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-83" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Gif\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Gif\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg" /v "EditFlags" /t REG_DWORD /d "65536" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3055" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-72" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg\shell\open" /v "MuiVerb" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\photoviewer.dll,-3043" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Jpg\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Png" /v "FriendlyTypeName" /t REG_EXPAND_SZ /d "@%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll,-3057" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Png" /v "ImageOptionFlags" /t REG_DWORD /d "1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Png\DefaultIcon" /ve /t REG_SZ /d "%%SystemRoot%%\System32\imageres.dll,-71" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Png\shell\open\command" /ve /t REG_EXPAND_SZ /d "%%SystemRoot%%\System32\rundll32.exe \"%%ProgramFiles%%\Windows Photo Viewer\PhotoViewer.dll\", ImageView_Fullscreen %%1" /f >nul
Reg add "HKLM\WIM_Software\Classes\PhotoViewer.FileAssoc.Png\shell\open\DropTarget" /v "Clsid" /t REG_SZ /d "{FFE2A43C-56B9-4bf5-9A79-CC6D4285608A}" /f >nul
Reg add "HKLM\WIM_Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".bmp" /t REG_SZ /d "PhotoViewer.FileAssoc.Bmp" /f >nul
Reg add "HKLM\WIM_Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".gif" /t REG_SZ /d "PhotoViewer.FileAssoc.Gif" /f >nul
Reg add "HKLM\WIM_Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".jpg" /t REG_SZ /d "PhotoViewer.FileAssoc.Jpg" /f >nul
Reg add "HKLM\WIM_Software\Microsoft\Windows Photo Viewer\Capabilities\FileAssociations" /v ".png" /t REG_SZ /d "PhotoViewer.FileAssoc.Png" /f >nul
Reg add "HKLM\WIM_Software\Classes\.jpg" /ve /t REG_SZ /d "PhotoViewer.FileAssoc.Jpg" /f
Reg add "HKLM\WIM_Software\Classes\.jpeg" /ve /t REG_SZ /d "PhotoViewer.FileAssoc.Jpg" /f
Reg add "HKLM\WIM_Software\Classes\.gif" /ve /t REG_SZ /d "PhotoViewer.FileAssoc.Gif" /f
Reg add "HKLM\WIM_Software\Classes\.png" /ve /t REG_SZ /d "PhotoViewer.FileAssoc.Png" /f
Reg add "HKLM\WIM_Software\Classes\.bmp" /ve /t REG_SZ /d "PhotoViewer.FileAssoc.Bmp" /f

reg.exe unload "HKLM\WIM_Software"
pause