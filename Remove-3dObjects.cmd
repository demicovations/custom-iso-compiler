@echo off
pushd "%~dp0"
reg.exe load "HKLM\WIM_Software" "wimmount\Windows\System32\config\software"
reg delete "HKLM\WIM_Software\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}" /f
reg delete "HKLM\WIM_Software\WOW6432Node\Microsoft\Windows\CurrentVersion\Explorer\MyComputer\NameSpace\{0DB7E03F-FC29-4DC6-9020-FF41B59E513A}" /f
reg.exe unload "HKLM\WIM_Software"