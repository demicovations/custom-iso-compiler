@echo off
cd %~dp0
title After install config


cls
echo First turn on extensions and disable indexing
timeout /t 2 /nobreak >nul
start explorer.exe
pause

cls
call explorer.exe ms-settings:windowsupdate
pause

cls
echo Starting ninite
call niniteIE.exe
pause

cls
call rundll32.exe shell32.dll,Control_RunDLL desk.cpl,,0
pause


cls
call s10\OOSU10.exe
pause

cls
echo starting PowerShell script
powershell -executionpolicy bypass -file ps.ps1
pause

cls
echo You can do trimming
start dfrgui.exe
pause

cls
echo Cleaning Default user profile
rd /s /q C:\Users\Default\Desktop\pkg
echo You can now delete this folder
timeout /t 3 /nobreak >nul