@echo off
cd %~dp0
set "home=%~dp0"
md iso_extract
md wimmount
title Custom ISO compiler
goto ImageMount
:ImageMount
::Image Mont
if not exist "iso_extract\sources\install.wim" goto :WimNotFound
cls
set "index=-1"
dism /Get-WimInfo /WimFile:iso_extract\sources\install.wim
set /p "index=Choose Windows index: "
if index==-1 (goto ImageMount)
title Mounting wim image...
DISM /mount-wim /wimfile:iso_extract\sources\install.wim /index:%index% /mountdir:wimmount


::pkg Copy
title Copying post install actions...
md wimmount\Users\Default\Desktop\pkg
md wimmount\Users\Default\Desktop\pkg\s10
copy pkg\* wimmount\Users\Default\Desktop\pkg
copy pkg\s10\* wimmount\Users\Default\Desktop\pkg\s10

::Bring back old photo viewer
title Bringing back phoroviewer...
call "PhotoViewer.cmd"

::Copy start menu and taskbar layout
title Copying start layout...
del /q wimmount\Users\Default\AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml
::copy LayoutModification.xml wimmount\Users\Default\AppData\Local\Microsoft\Windows\Shell\LayoutModification.xml

::Disables creating edge on the desktop
title Disabling edge on desktop...
call "DisableEdgeDesktopShortcutCreation.cmd"

::Disables "3D Objects" folder in File explorer
title Disabling 3D Objects folders...
call "Remove-3dObjects.cmd"

::Disable web searching in start menu
title Disabling web search in start menu...
call "web_search.cmd"

::Unmount wim image
title Unmounting image...
DISM /unmount-wim /mountdir:wimmount /commit

::Optimises Wim file
title Optimizing image...
wimlib optimize iso_extract\sources\install.wim

::Compile files to ISO
title Compiling Image...
oscdimg -h -m -o -u2 -udfver102 -bootdata:2#p0,e,biso_extract\boot\etfsboot.com#pEF,e,biso_extract\efi\microsoft\boot\efisys.bin -lWin10 iso_extract Custom_ISO.iso
title End
pause
goto END


:WimNotFound
cls
echo ISO is not extracted properly, terminating script...
timeout /t 3 /nobreak >nul
goto END
:END