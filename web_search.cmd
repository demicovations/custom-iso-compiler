@echo off
cd %~dp0
reg.exe load "HKLM\WIM_Software" "wimmount\Windows\System32\config\software"
reg add HKLM\WIM_Software\Microsoft\Windows\CurrentVersion\Search /v "BingSearchEnabled" /d 0 /t REG_DWORD /f
reg add HKLM\WIM_Software\Microsoft\Windows\CurrentVersion\Search /v "DisableWebSearch" /d 1 /t REG_DWORD /f
reg.exe unload "HKLM\WIM_Software"