@echo off
pushd "%~dp0"
reg.exe load "HKLM\WIM_Software" "wimmount\Windows\System32\config\software"
reg add HKLM\WIM_Software\Microsoft\Windows\CurrentVersion\Explorer /v "DisableEdgeDesktopShortcutCreation" /d 1 /t REG_DWORD /f
reg.exe unload "HKLM\WIM_Software"